<?php
    class WorkShopConst{
        public const USER = 'user';
        public const ACTION = 'action';
        #DataBase
        public const DBUSER = 'taller-user';
        public const DBPASS = 'taller123';
        public const DBNAME = 'tallerchzdb';
        #Client Data
        public const IDNUMBER = 'idnumber';
        public const NAME = 'name';
        public const CONTACT = 'contact';
        public const ADDRESS = 'address';
        public const PASS = 'pass';
        # Constants for Views/Modules
        public const HOME = 'home';
        public const HEADER = 'header';
        public const BODY = 'body';
        public const FOOTER = 'footer';
        public const SERVICE = 'service';
        public const QUERYSERVICE = 'queryservice';
        public const BOOKAPPOINTMENT = 'bookappointment';
        public const LOGIN = 'login';
        public const SIGNUP = 'signup';
        public const WHITELIST = array(
            'home' => 'home',
            'header' => 'header',
            'body' => 'body',
            'footer' => 'footer',
            'service' => 'service',
            'queryservice' => 'queryservice',
            'bookappointment' => 'bookappointment',
            'login' => 'login',
            'signup' => 'signup',
            'vehicles' => 'vehicles',
            'regVehicle' => 'regVehicle',
            'models' => 'models',
            'divisions' => 'divisions'
        );
    }
?>