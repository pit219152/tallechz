const HOMEURL = '';
const SERVICE = 'service';
const QUERYSERVICE = 'queryservice';
const BOOKAPPOINTMENT = 'bookappointment';
const VEHICLES = 'vehicles';
const LOGIN = 'login';
const LOGOUT = 'logout';

$(document).ready(function(){
    $("#servicebtn").click(() => {
        sendData(SERVICE);
    });

    $("#queryservicebtn").click(() => {
        sendData(QUERYSERVICE);
    });

    $("#bookappointmentbtn").click(() => {
        sendData(BOOKAPPOINTMENT);
    });

    $("#loginbtn").click(() => {
        sendData(LOGIN);
    });

    $("#vehiclesbtn").click(() => {
        sendData(VEHICLES);
    });

    $("#logoutbtn").click(() => {
        $.ajax(
            {
                url: HOMEURL,
                data: {
                    format: 'json',
                    action: LOGOUT
                },
                error: () => {
                    $("#body").html("Error");
                },
                success: function(data){
                    $("#body").html(data);
                    window.location.replace("");
                },
                type: 'POST'
            });
    });

    function sendData(sAction){
        $.ajax(
            {
                url: HOMEURL,
                data: {
                    format: 'json',
                    action: sAction
                },
                error: () => {
                    $("#body").html("Error");
                },
                success: function(data){
                    $("#body").html(data);
                },
                type: 'POST'
            });
    }
});
/*
td[name ="tcol1
$.ajax({
  url: 'http://api.joind.in/v2.1/talks/10889',
  data: {
    format: 'json'
  },
  error: function() {
    $('#info').html('<p>An error has occurred</p>');
  },
  dataType: 'jsonp',
  success: function(data) {
    var $title = $('<h1>').text(data.talks[0].talk_title);
    var $description =  $('<p>').text(data.talks[0].talk_description);
    $('#info')
    .append($title)
    .append($description);
  },
  type: 'GET'
});
*/