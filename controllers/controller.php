<?php

    class WorkshopController{

        # Elementos de Sesión
        public function filterUser()
        {
            if (session_status() == PHP_SESSION_NONE)   session_start();

            if(isset($_SESSION[WorkShopConst::USER]) && isset($_POST[WorkShopConst::ACTION])){
                $this->clientOptions();
                # Aqui dependiendo del tipo de usuario se abrira una funcionalidad u otra
            }else if(isset($_POST[WorkShopConst::ACTION])){
                    $this->publicOptions();
            }else{
                // Consultar servicios
                $mysqlcon = new MySqlModel(WorkShopConst::DBUSER, WorkShopConst::DBPASS, WorkShopConst::DBNAME);
                $services = $mysqlcon->queryServices();
                include WorkshopModel::getModule(WorkShopConst::HOME);
                $mysqlcon->closeConection();
            } 
            
        }

        private function clientOptions()
        {
            if(isset($_POST[WorkShopConst::ACTION])){
                $action = $_POST[WorkShopConst::ACTION];
                $mysqlcon = new MySqlModel(WorkShopConst::DBUSER, WorkShopConst::DBPASS, WorkShopConst::DBNAME);
                switch($action){
                    case 'logout':
                        session_destroy();
                        echo 'Sesion cerrada';
                    break;
                    case 'regVehicle':
                        $divisions = $mysqlcon->queryDivisions();
                        $models = $mysqlcon->queryModels('111000');
                        include WorkshopModel::getModule($action);
                    break;
                    case 'vehicles':
                        $vehicles = $mysqlcon->queryVehicles($_SESSION[WorkShopConst::IDNUMBER]);
                        include WorkshopModel::getModule($action);
                    break;
                    case 'models':
                        $division = $_POST['division'];
                        $models = $mysqlcon->queryModels($division);
                        include WorkshopModel::getModule('models');
                    break;
                    case 'bookappointment':
                        $appointment = $mysqlcon->queryAppointment($_SESSION[WorkShopConst::IDNUMBER]);
                        include WorkshopModel::getModule('bookappointment');
                    break;
                    case 'sbookappointment':
                        $date = $_POST['sdate'];
                        $owner = $_SESSION[WorkShopConst::IDNUMBER];
                        
                        $appointment = $mysqlcon->insertAppointment($date, $owner);
                        if($appointment){
                            $data = ['result'=> true, 'messagge' => 'Turno registrado con éxito '];
                        }else{
                            $data = ['result'=> false, 'messagge' => 'Error registrando turno'];
                        }
                        echo json_encode($data);
                    break;
                    case 'ubookappointment':
                        $date = $_POST['sdate'];
                        $owner = $_SESSION[WorkShopConst::IDNUMBER];
                        $appointment = $mysqlcon->updateAppointment($date, $owner);
                        if($appointment){
                            $data = ['result'=> true, 'messagge' => 'Turno modificado con éxito '];
                        }else{
                            $data = ['result'=> false, 'messagge' => 'Error modificando turno'];
                        }
                        echo json_encode($data);
                    break;
                    case 'cbookappointment':
                        $owner = $_SESSION[WorkShopConst::IDNUMBER];
                        $appointment = $mysqlcon->deleteAppointment($owner);
                        if($appointment){
                            $data = ['result'=> true, 'messagge' => 'Turno cancelado'];
                        }else{
                            $data = ['result'=> false, 'messagge' => 'Error cancelando turno'];
                        }
                        echo json_encode($data);
                    break;

                    case 'divisions':
                        $divisions = $mysqlcon->queryDivisions();
                        include WorkshopModel::getModule('divisions');
                    break;

                    case 'selectedmodels':
                        $smod = $_POST['smod'];
                        $sdiv = $mysqlcon->queryDivisionSelected($smod);
                        $models = $mysqlcon->queryModels($sdiv);
                        include WorkshopModel::getModule('models');
                    break;

                    case 'upvehicle':
                        $plate = $_POST['plate'];
                        $model = $_POST['model']; 
                        $mileage = $_POST['mileage']; 
                        if($mysqlcon->updateVehicle($plate, $model, $mileage, $_SESSION[WorkShopConst::IDNUMBER])){
                            $data = ['result'=> true, 'messagge' => 'Vehículo modificado con éxito'];
                        }else $data = ['result'=> false, 'messagge' => 'Error en actualización'];
                        echo json_encode($data);
                    break;

                    case 'delvehicle':
                        $plate = $_POST['plate'];
                        if($mysqlcon->deleteVehicle($plate, $_SESSION[WorkShopConst::IDNUMBER])){
                            $data = ['result'=> true, 'messagge' => 'Vehículo eliminado con éxito'];
                        }else $data = ['result'=> false, 'messagge' => 'Error en operación'];
                        echo json_encode($data);
                    break;

                    case 'reg':
                        $plate = $_POST['plate'];
                        $model = $_POST['model']; 
                        $mileage = $_POST['mileage']; 
                        if($mysqlcon->insertVehicle($plate, $model, $mileage, $_SESSION[WorkShopConst::IDNUMBER])){
                            $data = ['result'=> true, 'messagge' => 'Vehículo registrado con éxito'];
                        }else $data = ['result'=> false, 'messagge' => 'Error en Registro'];
                        echo json_encode($data);
                    break;

                    case 'service':
                        $services = $mysqlcon->queryServices();
                        include WorkshopModel::getModule(WorkShopConst::SERVICE);
                    break;

                    case 'queryservice':
                        $works = $mysqlcon->getActiveWorks($_SESSION[WorkShopConst::IDNUMBER]);
                        include WorkshopModel::getModule(WorkShopConst::QUERYSERVICE);
                    break;

                    case 'events':
                        $turnos = $mysqlcon->getAsignedappointments($_SESSION[WorkShopConst::IDNUMBER]);
                        $events = [];
                        $i = 0;
                        foreach($turnos as $turno){
                            $events[$i] = ['title'=> 'Ocupado', 'start' => $turno, 'color'=>'red', 'textColor'=>'FFFFFF'];
                            $i += 1;
                        }
                        echo json_encode($events);
                    break;

                }
                $mysqlcon->closeConection();
            }
        }

        private function publicOptions(){
            $action = $_POST[WorkShopConst::ACTION];
            switch($action){
                case 'log':
                    $this->logClient();
                    break;
                case 'sign':
                    $this->signClient();
                    break;
                default:
                    include WorkshopModel::getModule($action);
                    break;
            }
        }

        private function logClient(){
            $idnumber = $_POST[WorkShopConst::IDNUMBER];
            $pass = $_POST[WorkShopConst::PASS];
            $mysqlcon = new MySqlModel(WorkShopConst::DBUSER, WorkShopConst::DBPASS, WorkShopConst::DBNAME);

            if($mysqlcon->isEnableConnection()){
                $user = $mysqlcon->validateClient($idnumber, $pass);
                if($user){
                    $data = ['result'=> true, 'messagge' => 'Inisio de Sesión Exitoso'];
                    #Crear Sesión y Redirigir
                    $_SESSION[WorkShopConst::USER] = $user['name'];
                    $_SESSION[WorkShopConst::IDNUMBER] = $user['idnumber'];
                }else{
                    $data = ['result'=> false, 'messagge' => 'Credenciales Incorrectas'];
                }
                echo json_encode($data);
            }
            $mysqlcon->closeConection();
        }

        private function signClient(){
            $idnumber = $_POST[WorkShopConst::IDNUMBER];
            $name = $_POST[WorkShopConst::NAME];
            $address = $_POST[WorkShopConst::ADDRESS];
            $contact = $_POST[WorkShopConst::CONTACT];
            $pass = $_POST[WorkShopConst::PASS];

            $mysqlcon = new MySqlModel(WorkShopConst::DBUSER, WorkShopConst::DBPASS, WorkShopConst::DBNAME);
            if($mysqlcon->isEnableConnection()){
                if($mysqlcon->insertClient($idnumber, $name, $address, $contact, $pass)){
                    echo 'Se inserto el cliente';
                    $_SESSION[WorkShopConst::USER] = $name;
                    $_SESSION[WorkShopConst::IDNUMBER] = $idnumber;
                    #Crear Sesión y Redirigir
                }
            }
            $mysqlcon->closeConection();
        }

        private function getVehicles(){
            $mysqlcon = new MySqlModel(WorkShopConst::DBUSER, WorkShopConst::DBPASS, WorkShopConst::DBNAME);
            $vehicles = $mysqlcon->queryVehicles($_SESSION[WorkShopConst::IDNUMBER]);
            include WorkshopModel::getModule($action);
            #Consultar vehiculos y mostrar en pantalla
        }

    }

?>