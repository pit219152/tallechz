<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="public/css/bootstrap.css">
    <link rel="stylesheet" href="public/css/mystyles.css">
    <link rel="stylesheet" href="public/css/fullcalendar.min.css">
    <title>Inicio</title>
</head>
<body>
    <div>
        <?php
            include WorkshopModel::getModule(WorkShopConst::HEADER);
        ?>
    </div>
    <div>
        <?php
            include WorkshopModel::getModule(WorkShopConst::BODY);
        ?>
    </div>
    <div>
        <?php
            include WorkshopModel::getModule(WorkShopConst::FOOTER);
        ?>
    </div>
    <!--Scripts-->
    <script src="public/js/jquery.js"></script>
    <script src="public/js/myjquery.js"></script>
    <script src="public/js/bootstrap.js"></script>
</body>
</html>