<div id="servicescont" class="container d-flex justify-content-center">
<?php
if(count($works)>0){
        foreach($works as $work){
?>
        <div class="card col-md-4">
            <div class="card-body">
                <h5 class="card-title"><?php echo $work['vehiculo']?></h5>
                <p class="card-text"><b>Estatus: <?php echo $work['estatus']?></b></p>
                <p class="card-text"><b>Fecha inicio: <?php echo $work['fechai']?></b></p>
                <p class="card-text"><b>Fecha entrega: <?php echo $work['fechae']?></b></p>
            </div>
        </div>
<?php
        }
}else{
        echo('<div> NO tiene servicios activos </div>\n');
}
?>
</div>