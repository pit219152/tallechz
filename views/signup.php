<div class="container padded">
    <form id="myform" action="signup" method="POST">
        <div class="row imputrow">    
                <label for="idnumber" class="col-sm-6 col-md-6 d-flex justify-content-center">Número de Identificación</label>
                <input name="idnumber" id="idnumber" class="col-sm-6 col-md-6 d-flex justify-content-center" type="text" required>
        </div>
        <div class="row imputrow">
                <label for="name" class="col-sm-6 col-md-6 d-flex justify-content-center">Nombres y Apellidos</label>
                <input type="text" id="name" class="col-sm-6 col-md-6 d-flex justify-content-center"name="name" required>
        </div>
        <div class="row imputrow">
                <label for="address" class="col-sm-6 col-md-6 d-flex justify-content-center">Dirección</label>
                <input type="text" id="address" class="col-sm-6 col-md-6 d-flex justify-content-center" name="address" required>    
        </div>
        <div class="row imputrow">
            <label for="contact" class="col-sm-6 col-md-6 d-flex justify-content-center">Número de Contacto</label>
            <input type="number" id="contact" class="col-sm-6 col-md-6 d-flex justify-content-center" name="contact" required>
        </div>
        <div class="row imputrow">
            <label for="pass" class="col-sm-6 col-md-6 d-flex justify-content-center">Contraseña</label>
            <input type="password" id="pass" class="col-sm-6 col-md-6 d-flex justify-content-center" name="pass" required>
        </div>
        <div class="row imputrow">
            <label for="confpass" class="col-sm-6 col-md-6 d-flex justify-content-center">Confirmar Contraseña</label>
            <input type="password" id="confpass" class="col-sm-6 col-md-6 d-flex justify-content-center" name="confpass" required>
        </div>
        <br>        
    </form>
    <div class="row justify-content-center">
        <button id="signup" class="btn btn-light">Inscribir</button>
        <button id="clear" class="btn btn-light">Limpiar</button>
    </div>
    <br>
    <div id="res" class="d-flex justify-content-center whiteText">
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#clear").click(() => {
            $("#myform").trigger("reset");
    });

    $("#signup").click(() => {
        //Aqui se debe hacer una validacion antes de enviar
        if(!validateData()) return;
        $.ajax(
            {
                url: HOMEURL,
                data: {
                    format: 'json',
                    action: 'sign',
                    idnumber: $("#idnumber").val(),
                    name: $("#name").val(),
                    contact: $("#contact").val(),
                    address: $("#address").val(),
                    pass: $("#pass").val()
                },
                error: () => {
                    $("#res").html("Error");
                },
                success: function(data){
                    $("#res").html(data);
                },
                type: 'POST'
            });
    });

    function validateData(){
        requiredVal = "";
        if($("#idnumber").val().length === 0){
            requiredVal = "Se requiere un número de identificación";
        }
        else if($("#name").val().length === 0){
            requiredVal = "Se requiere un nombre";
        }
        else if($("#address").val().length === 0){
            requiredVal = "Se requiere una dirección";
        }
        else if($("#contact").val().length === 0){
            requiredVal = "Se requiere un número de contacto";
        }
        else if($("#pass").val().length === 0){
            requiredVal = "Se requiere una contraseña";
        }
        else if($("#confpass").val().length === 0){
            requiredVal = "Se requiere que confirme la contraseña";
        }else if($("#confpass").val() !== $("#pass").val()){
            requiredVal = "Las contraseñas no coinciden";
        }
        if(requiredVal.length !== 0){
            $("#res").html(requiredVal);
            return false;
        }
        return true;
    }
    function clearSometing(){
        $("form").reset();
    }

    });
</script>