<div class="container padded">
    <form id="myform" action="signup" method="POST">
        <div class="row imputrow">    
                <label for="plate" class="col-sm-6 col-md-6 d-flex justify-content-center">Placa del Vehiculo</label>
                <input name="plate" id="plate" class="col-sm-6 col-md-6 d-flex justify-content-center" type="text" required>
        </div>
        <div class="row imputrow">
                <label for="division" class="col-sm-6 col-md-6 d-flex justify-content-center">Marca del vehículo</label>
                <select name="division" id="division" class="col-sm-6 col-md-6 d-flex justify-content-center">
                <?php 
                        include WorkshopModel::getModule('divisions');
                ?> 
                </select>
        </div>
        <div class="row imputrow">
                <label for="model" class="col-sm-6 col-md-6 d-flex justify-content-center">Modelo del vehículo</label>
                <select name="model" id="model" class="col-sm-6 col-md-6 d-flex justify-content-center">
                    <?php 
                        include WorkshopModel::getModule('models');
                    ?> 
                </select>
        </div>
        <div class="row imputrow">
                <label for="mileage" class="col-sm-6 col-md-6 d-flex justify-content-center">Kilometraje del vehículo</label>
                <input type="number" placeholder="0" id="mileage" class="col-sm-6 col-md-6 d-flex justify-content-center" name="mileage" required >    
        </div>
        <br>        
    </form>
    <div class="row justify-content-center">
        <button id="reg" class="btn btn-light">Registrar</button>
        <button id="clear" class="btn btn-light">Limpiar</button>
    </div>
    <br>
    <div id="res" class="d-flex justify-content-center whiteText">
    </div>
</div>
<script>
$(document).ready(function(){
    $("#division").change(function(){
        var selectedDivision = $(this).children("option:selected").val();
        $.ajax(
        {
            url: HOMEURL,
            data: {
                format: 'json',
                action: 'models',
                division: selectedDivision
            },
            error: () => {
                $("#res").html("No encontrado");
            },
            success: function(data){
                $("#model").html(data);
            },
            type: 'POST'
        });
        
    });
    $("#reg").click(()=>{
        if(!validateData()) return;
        $plate = $("#plate").val();
        $model = $("#model").children("option:selected").val();
        if($("#mileage").val().lengt === 0) $mileage = 0;
        else $mileage = $("#mileage").val();
        $.ajax(
        {
            url: HOMEURL,
            data: {
                format: 'json',
                action: 'reg',
                plate : $plate, 
                model : $model, 
                mileage : $mileage 
            },
            error: () => {
                $("#res").html("Error!");
            },
            success: function(data){
                var dataRes = JSON.parse(data);
                    if(!dataRes.result){
                        $("#res").html(dataRes.messagge);
                    }else{
                        $("#body").html(dataRes.messagge);
                        window.location.replace("");
                    }
            },
            type: 'POST'
        });
    });

    function validateData(){
        requiredVal = "";
        if($("#plate").val().length === 0){
            requiredVal = "Se requiere una Placa";
        }else if($("#plate").val().length > 6 ){
            requiredVal = "Placa Invalida";
        }
        if(requiredVal.length !== 0){
            $("#res").html(requiredVal);
            return false;
        }
        return true;
    }
});
</script>