<div class="container padded">
    <form action="signup" method="POST">
        <div class="imputrow d-flex justify-content-center">    
                <label for="idnumber" class="col-sm-6 col-md-6 d-flex justify-content-center">Número de Identificación</label>
        </div>        
        <div class="d-flex justify-content-center">
                <input name="idnumber" id="idnumber" class="col-sm-6 col-md-6 d-flex justify-content-center" type="text" required>
        </div>
        <div class="imputrow d-flex justify-content-center">
            <label for="pass" class="col-sm-6 col-md-6 d-flex justify-content-center">Contraseña</label>
        </div>
        <div class="d-flex justify-content-center">
            <input type="password" id="pass" class="col-sm-6 col-md-6 d-flex justify-content-center" name="pass" required>
        </div>
        <br>        
    </form>
    <div class="row justify-content-center">
        <button id="login" class="btn btn-light">Iniciar Sesión</button>
    </div>
    <br>
    <div class="d-flex justify-content-center">
        <p class="whiteText">
            Si no tiene cuenta aún:
        </p>
    </div>
    <div class="d-flex justify-content-center">
        <button id="signup" class="al">Registrese aquí...</button>
    </div>
    <div id="res" class="d-flex justify-content-center whiteText">
    </div>
</div>
<script>
    $(document).ready(function(){

        $("#login").click(() => {
        if(!validateData()) return;
        $.ajax(
            {
                url: HOMEURL,
                data: {
                    format: 'json',
                    action: 'log',
                    idnumber: $("#idnumber").val(),
                    pass: $("#pass").val()
                },
                error: () => {
                    $("#res").html("Error");
                },
                success: function(data){
                    
                    var dataRes = JSON.parse(data);
                    if(!dataRes.result){
                        $("#res").html(dataRes.messagge);
                    }else{
                        $("body").html(dataRes.messagge);
                        window.location.replace("");
                    }
                    
                },
                type: 'POST'
            });
    });


    $("#signup").click(() => {
        $.ajax(
            {
                url: HOMEURL,
                data: {
                    format: 'json',
                    action: 'signup'
                },
                error: () => {
                    $("#res").html("Error");
                },
                success: function(data){
                    $("#body").html(data);
                },
                type: 'POST'
            });
    });

    function validateData(){
        requiredVal = "";
        if($("#idnumber").val().length === 0){
            requiredVal = "Se requiere un número de identificación";
        }
        else if($("#pass").val().length === 0){
            requiredVal = "Se requiere una contraseña";
        }
        if(requiredVal.length !== 0){
            $("#res").html(requiredVal);
            return false;
        }
        return true;
    }
    });
</script>