<div id="footer" class="container">
    <div class="row">
        <div class="col-md-3">
            <img src="public/logos/logotaller2.png" alt="TallerCHZ" height="250" width="250">
        </div>
        <div class="col-md-9">
            <h4>TallerCHZ</h4>
            <p>
                Es una organización que se enorgullece por su talento humano, contamos con técnicos mecánicos especializados en las nuevas tecnologías instruidos para dar solución a cualquier código de avería que presente su vehiculo ultimo modelo. E incluso mecánicos veteranos en la atención de vehiculos semi computarizados con el fin de dar atención a nuestra diversa clientela.
            </p>
            <p>
                Puede comunicarse con nosotros a través del XXX-XXX-XXX o a traves del correo example@exe.com.co
            </p>
        </div>
    </div>
</div>