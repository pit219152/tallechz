<?php if (session_status() == PHP_SESSION_NONE)   session_start(); ?>
<div id="header" class="container">
    <div>
        <img src="public/logos/logotaller4.png" alt="TallerCHZ" height="40" width="160">
    </div> 
    <div class="row">
        <!--Si existe Sesión -->
        <?php if(isset($_SESSION[WorkShopConst::USER])): ?>
            <div class="col-xs-12 col-sm-6 col-md-2 d-flex justify-content-center">
                <button id="servicebtn" class="hoption">Servicios</button>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-3 d-flex justify-content-center">
                <button id="vehiclesbtn" class="hoption">Vehiculos</button>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-3 d-flex justify-content-center">
                <button id="queryservicebtn" class="hoption">Consultar servicio activo</button>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-2 d-flex justify-content-center">
                <button id="bookappointmentbtn" class="hoption">Turnos</button>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-2 d-flex justify-content-center">
                <button id="logoutbtn" class="hoption">Cerrar Sesión</button>
            </div>
        <?php else: ?>
        <!--No existe Sesión -->
        <div class="col-xs-12 col-sm-6 col-md-6 d-flex justify-content-center">
            <button id="servicebtn" class="hoption">Servicios</button>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 d-flex justify-content-center">
            <button id="loginbtn" class="hoption">Iniciar Sesión</button>
        </div>
        <?php endif; ?>
    </div>
</div>

