<?php if($appointment == NULL): ?>
    <div class="backgrey">
        <p class="text-center">Ud aún no tiene un turno, seleccione <b>Solicitar Turno</b> para agendar uno. </p>
        <br>
        <div id="btns" class="d-flex justify-content-center">
            <button id ="dcallendar" type="button" class="btn btn-primary">Solicitar Turno</button>
        </div>
    </div>        
<?php else: ?>
    <div class="backgrey">
        <h4 class="text-center">La fecha de su turno es: <span id="curdate" class="text-secondary"><?php echo $appointment;?></span> </h4>
        <br>
        <p class="text-center">            
            Si desea modificar la fecha de su turno, presione el botón de <b>Modificar Turno</b><br>
            Si desea cancelar su turno, presione el botón de <b>cancelar turno</b><br>
            <span class="redtext">Recuerde que solo podrá modificar o cancelar su turno con un plazo mínimo de un día de antelación.</span>
        </p>
        <div id="btns" class="d-flex justify-content-center">
            <button id ="cancelApp" type="button" class="btn btn-danger">Cancelar Turno</button>
            <button id="setApp" type="button" class="btn btn-success">Modificar Turno</button>
        </div>
    </div>
<?php endif;?>
    <div class="backgrey">
            
        <div class="container">
            <div id="mess">

            </div>   
            <div class="row">
                <div class="col"></div>
                <div class="col-7">
                    <div id="datecalendar" class="backgrey"></div>
                </div>
                <div class="col"></div>
            </div>
        </div>
        <div id="res"></div>
        </div>

<!--SCRIPTS-CALENDAR-->
<script src="public/js/jquery.js"></script>
<script src="public/js/moment.min.js"></script>
<script src="public/js/fullcalendar.min.js"></script>
<script src="public/js/es.js"></script>
<script src="public/js/bootstrap.js"></script>

<!--JQUERY-->
<script>
    var flagSet = false;
    $(document).ready(()=>{
        $('#dcallendar').click(()=>{
            $('#dcallendar').prop('disabled', true);
            $('#mess').append('<h4 class="text-center">Seleccione la fecha para su turno</h4>');
            openCalendar();
        });

    $('#tim').change(()=>{
        validateTime();
    });

    $('#sApp').click(()=>{
        // Validar cuando value es 0
        secdate = $('#sDate').html()+" "+$('#tim').val();
        if(flagSet) act = 'ubookappointment';
        else act = 'sbookappointment';

        $.ajax(
        {
            url: HOMEURL,
            data: {
                format: 'json',
                action: act,
                sdate: secdate
            },
            error: () => {
                $("#res").html("Error gestionando su turno");
            },
            success: function(data){
                
                var dataRes = JSON.parse(data);
                if(!dataRes.result){
                    $("#res").html(dataRes.messagge);
                }else{
                    $("#res").html(dataRes.messagge);
                    window.location.replace("");
                }
            },
            type: 'POST'
        });
                
    });

    $("#setApp").click(()=>{
        flagSet = true;
        openCalendar();
        var evento = {
            title: 'Turno actual',
            start: $('#curdate').html(),
            color:'green',
            textColor: 'FFFFFF'

        };
        $('#datecalendar').fullCalendar('renderEvent',evento, true);
    });

    $('#cancelApp').click(()=>{
        if(confirm("¿Esta seguro de querer cancelar su turno?")){
            $.ajax(
            {
                url: HOMEURL,
                data: {
                    format: 'json',
                    action: 'cbookappointment',
                },
                error: () => {
                    $("#res").html("Error gestionando su turno");
                },
                success: function(data){
                    
                    var dataRes = JSON.parse(data);
                    if(!dataRes.result){
                        $("#res").html(dataRes.messagge);
                    }else{
                        $("#res").html(dataRes.messagge);
                        window.location.replace("");
                    }
                },
                type: 'POST'
            });
        }
    });

    });

    function validateTime(){
            if($('#tim').val() < '07:00' || $('#tim').val() > '17:00'){
                $('#warn').html("Hora Invalida, Horario de atención de 7:00am a 5:00pm");
                $('#sApp').prop('disabled', true);
            }else{
                $('#sApp').prop('disabled', false);
                $('#warn').html("");
            }
    }

    function openCalendar(){
        $('#datecalendar').fullCalendar({
                lang:'es',
                header:{
                    left: 'today, prev, next',
                    center: 'title',
                    right: 'month, agendaWeek'
                },
                dayClick: function(date, jsEvent, view){
                    var curdate = new Date();
                    var otdate = new Date(date.format());
                    if(otdate <= curdate || otdate.getDay() == 5 || otdate.getDay() == 6) return;
                    
                    if(flagSet){
                        $('#modalTitle').html('Mover turno a...');
                        $('#sApp').text('Modificar Turno');
                    }
                    if(view =='month'){
                        $('#sApp').prop('disabled', true);
                        $('#sDate').html(date.format());
                    }else{
                        tmp = date.format();
                        selDate = tmp.split('T');
                        $('#sDate').html(selDate[0]);
                        $('#tim').val(selDate[1]);
                        validateTime();
                    }
                    $('#appointmentModal').modal();
                    
                },
                eventClick: (calEvent, jsEvent, view) =>{
                    alert("Fecha no disponible");
                }
            });
        $.ajax({
            url: HOMEURL,
                data: {
                    format: 'json',
                    action: 'events',
                },
                error: () => {
                    $("#res").html("Error gestionando su turno");
                },
                success: function(data){
                    
                    var dataRes = JSON.parse(data);
                    for(i=0; i<dataRes.length; i+=1){
                        evento = {
                            title: dataRes[i]['title'],
                            start: dataRes[i]['start'],
                            color: dataRes[i]['color'],
                            textColor: dataRes[i]['textColor']
                        };
                        $('#datecalendar').fullCalendar('renderEvent',evento, true);
                        
                        //$('#res').html(dataRes[i]['title']);
                    }
                },
                type: 'POST'
        });
           
    }
</script>
<div class="modal fade" id="appointmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="modalTitle">Solicite su turno</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" id="modalBody">
            <h5 >Fecha seleccionada: </h5>
            <p id="sDate">Fecha seleccionada </p>
            <div class="formgroup">
                <label for="tim"><b>Hora </b></label>
                <input id="tim" type="time" min="07:00" max="18:00" step="3600000">
            </div>
            <p id="warn" class="redtext"></p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button id="sApp" type="button" class="btn btn-primary" data-dismiss="modal">Solicitar Turno</button>
        </div>
    </div>
    </div>
</div>

