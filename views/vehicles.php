<div class="container padded">
    <?php if($vehicles): ?>
        <table class="table">
            <thead>
                <tr class="table-secondary">
                    <th scope="col">Placa</th>
                    <th scope="col">Cod.marca</th>
                    <th scope="col">Marca</th>
                    <th scope="col">Cod.modelo</th>
                    <th scope="col">Modelo</th>
                    <th scope="col">Kilometraje</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php
                foreach($vehicles as $vehicle){
            ?>
                <tr class="table-light">
                    <td id="plate<?php echo $vehicle['plate']; ?>" scope="col"><?php echo $vehicle['plate']; ?></td>
                    <td id="coddivision<?php echo $vehicle['plate']; ?>" scope="col"><?php echo $vehicle['coddivision']; ?></td>
                    <td id="nomdivision<?php echo $vehicle['plate']; ?>" scope="col"><?php echo $vehicle['nomdivision']; ?></td>
                    <td id="codmodel<?php echo $vehicle['plate']; ?>" scope="col"><?php echo $vehicle['codmodel']; ?></td>
                    <td id="nommodel<?php echo $vehicle['plate']; ?>" scope="col"><?php echo $vehicle['nommodel']; ?></td>
                    <td id="mileage<?php echo $vehicle['plate']; ?>" scope="col"><?php echo $vehicle['mileage']; ?></td>
                    <td id="btn<?php echo $vehicle['plate']; ?>" scope="col">
                        <button id="<?php echo $vehicle['plate']; ?>" class="btn btn-warning mod">Modificar</button>
                    </td>
                </tr>
            <?php   }
            ?>
            </tbody>
        </table>
    <?php endif; ?>
    <div class="d-flex justify-content-center">
            <p class="whiteText">
                Para registar un nuevo vehiculo
            </p>
        </div>
        <div class="d-flex justify-content-center">
            <button id="regVehicle" class="al">Clic aquí...</button>
    </div>
</div>
<div id="res" class="d-flex justify-content-center whiteText">
</div>
<script>
    $(document).ready(function(){
        $("#regVehicle").click(() => {
            $.ajax(
            {
                url: HOMEURL,
                data: {
                    format: 'json',
                    action: 'regVehicle',
                },
                error: () => {
                    $("#res").html("Error");
                },
                success: function(data){
                    $("#body").html(data);
                },
                type: 'POST'
            });
        });
    });

    $("button.mod").click(function(){
        idv = $(this).attr('id');
        $('#vehiclemodal').modal();
        $('#platev').html($('#plate'+idv).html());
        $('#mileagev').val($('#mileage'+idv).html());
        divi = $('#coddivision'+idv).html();
        mountdiv(divi);
        mountmol(divi);
    });

    $('#setv').click(function(){
        


        idv = $('#platev').html();
        var selectedcodDivision = $('#divisionv').children("option:selected").val();
        var selectednomDivision = $('#divisionv').children("option:selected").html();
        var selectedcodModel = $('#modelv').children("option:selected").val()
        var selectednomModel = $('#modelv').children("option:selected").html();
        var curmileage = $('#mileagev').val();
        
        $.ajax(
        {
            url: HOMEURL,
            data: {
                format: 'json',
                action: 'upvehicle',
                plate: idv,
                model: selectedcodModel,
                mileage: curmileage
            },
            error: () => {
                $("#res").html("No encontrado");
            },
            success: function(data){
                
                var dataRes = JSON.parse(data);
                    if(!dataRes.result){
                        $("#res").html(dataRes.messagge);
                    }else{
                        $("#res").html(dataRes.messagge);
                        $('#coddivision'+idv).html(selectedcodDivision);
                        $('#nomdivision'+idv).html(selectednomDivision);
                        $('#codmodel'+idv).html(selectedcodModel);
                        $('#nommodel'+idv).html(selectednomModel);
                        $('#mileage'+idv).html(curmileage);
                    }
            },
            type: 'POST'
        });
        
    });

    $('#delv').click(function(){
        $.ajax(
        {
            url: HOMEURL,
            data: {
                format: 'json',
                action: 'delvehicle',
                plate: $('#platev').html()
            },
            error: () => {
                $("#res").html("No encontrado");
            },
            success: function(data){
                var dataRes = JSON.parse(data);
                $("#res").html(dataRes.messagge);
                if(dataRes.result){
                    window.location.replace("");
                }
            },
            type: 'POST'
        });
    });

    $("#divisionv").change(function(){
        var selectedDivision = $(this).children("option:selected").val();
        $.ajax(
        {
            url: HOMEURL,
            data: {
                format: 'json',
                action: 'models',
                division: selectedDivision
            },
            error: () => {
                $("#res").html("No encontrado");
            },
            success: function(data){
                $("#modelv").html(data);
            },
            type: 'POST'
        });
        
    });

    function mountdiv(divi){
        $.ajax(
            {
                url: HOMEURL,
                data: {
                    format: 'json',
                    action: 'divisions',
                },
                error: () => {
                    $("#res").html("Error");
                },
                success: function(data){
                    $('#divisionv').html(data);
                    $('#'+divi).attr("selected", true);
                },
                type: 'POST'
            });
    }

    function mountmol(div){
        $.ajax(
            {
                url: HOMEURL,
                data: {
                    format: 'json',
                    action: 'models',
                    division: div
                },
                error: () => {
                    $("#res").html("Error");
                },
                success: function(data){
                    $('#modelv').html(data);
                    model = $('#codmodel'+idv).html();
                    $('#'+model).attr("selected", true);
                },
                type: 'POST'
            });
    }
</script>
<div class="modal fade" id="vehiclemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="modalTitle">Modificar Vehículo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" id="modalBody">
            
            <div class="formfoup row">
                <p class="col-sm-12">Modificando:<span id="platev"></span></p>
            </div>
            
            <div class="formfoup row">
                <label for="divisionv" class="col-sm-6">Marca:</label>
                <select name="divisionv" id="divisionv" class="col-sm-6 col-md-6 d-flex justify-content-center">
                </select>
            </div>
            <div class="formfoup row">
                <label for="modelv" class="col-sm-6">Modelo:</label>
                <select name="modelv" id="modelv" class="col-sm-6 col-md-6 d-flex justify-content-center">
                </select>
            </div>
            <div class="formfoup row">
                <label for="mileagev" class="col-sm-6">kilometraje:</label>
                <input id="mileagev" name="mileagev" type="number" class="col-sm-6">
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button id="delv" type="button" class="btn btn-danger" data-dismiss="modal">Eliminar</button>
        <button id="setv" type="button" class="btn btn-primary" data-dismiss="modal">Modificar</button>
        </div>
    </div>
    </div>
</div>
