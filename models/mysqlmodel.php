<?php
    class MySqlModel{
        private $mysqli;
        
        public function MySqlModel($user, $pass, $db){
            $this->mysqli = new mysqli('localhost', $user, $pass, $db);
        }

        public function isEnableConnection(){
            if (!$this->mysqli->connect_errno)  return True;
            return False;
        }

        public function validateClient($idnumber, $pass){
            if($stmt = $this->mysqli->prepare("SELECT idnumber, name, pass FROM clientes WHERE idnumber = ? ;"))
            {
                if($stmt->bind_param('s',$idnumber)){
                    if($stmt->execute()){
                        $id = NULL;
                        $name = NULL;
                        $npass = NULL;
                        
                        if ($stmt->bind_result($id, $name, $npass)){

                            while($stmt->fetch()){
                                if(password_verify($pass, $npass)){
                                    return [ 'idnumber'=> $id, 'name' => $name];
                                }
                            }
                        }
                    }else echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                }else echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            } else echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            return NULL;
        }


        public function queryVehicles($owner){
            $str = "SELECT plate, coddivision, nomdivision, codmodelo, nommodelo, mileage FROM vehiculos JOIN modelos ON model=codmodelo JOIN divisiones ON division=coddivision WHERE owner= ? ;";
            $stmt = $this->mysqli->prepare($str);
            $stmt->bind_param('s',$owner);
            $stmt->execute();

            $plate = NULL;
            $coddivision = NULL;
            $nomdivision = NULL;
            $codmodel = NULL;
            $nommodel = NULL;
            $mileage = NULL;

            $vehicles = [];
            $i= 0;
            $stmt->bind_result($plate, $coddivision, $nomdivision, $codmodel, $nommodel,$mileage);
            while($stmt->fetch()){
                $vehicles[$i] = ['plate' => $plate,
                'coddivision' => $coddivision,
                'nomdivision' => $nomdivision,
                'codmodel' => $codmodel,
                'nommodel' => $nommodel,
                'mileage' => $mileage];
                $i +=1;
            }
            return $vehicles;
        }

        public function queryDivisions(){
            if($stmt = $this->mysqli->prepare("SELECT coddivision,nomdivision FROM divisiones;"))
            {
                if($stmt->execute()){
                    $coddivision = NULL;
                    $nomdivision = NULL;
                    $divisions = [];
                    $i = 0;
                    if ($stmt->bind_result($coddivision, $nomdivision)){
                        while($stmt->fetch()){
                            $divisions[$i] = ['coddivision' => $coddivision, 'nomdivision' => $nomdivision];
                            $i +=1;
                        }
                        return $divisions;
                    }
                }else echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            } else echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            return NULL;
        }

        public function queryModels($division){
    
            if($stmt = $this->mysqli->prepare("SELECT codmodelo, nommodelo FROM modelos WHERE division= ? ;"))
            {
                if($stmt->bind_param('s',$division)){
                    if($stmt->execute()){
                        $codmodelo = NULL;
                        $nommodelo = NULL;
                        $models = [];
                        $i = 0;
                        if ($stmt->bind_result($codmodelo, $nommodelo)){
                            while($stmt->fetch()){
                                $models[$i] = ['codmodelo' => $codmodelo, 'nommodelo' => $nommodelo];
                                $i +=1;
                            }
                            return $models;
                        }
                    }else echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                }else echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            } else echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            return NULL;
        }
        
        public function queryAppointment($owner){
    
            if($stmt = $this->mysqli->prepare("SELECT date FROM turnos WHERE owner=?"))
            {
                if($stmt->bind_param('s',$owner)){
                    $appointment = NULL;
                    if($stmt->execute()){
                        $date = NULL;
                        if ($stmt->bind_result($date)){
                            while($stmt->fetch()){
                                $appointment = $date;
                            }
                            return $appointment;
                        }
                    }else echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                }else echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            } else echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            return NULL;
        }

        public function insertClient($idnumber, $name, $address, $contact, $pass){
            $stmt = $this->mysqli->prepare("INSERT INTO clientes VALUES (?, ?, ?, ?, ?)");
            $crip_pass = password_hash($pass, PASSWORD_DEFAULT);
            $stmt->bind_param('sssss',$idnumber, $name, $address, $contact, $crip_pass);
            $stmt->execute();
            if($stmt)   return true;
            return false;
        }

        public function insertAppointment($date, $owner){
            $stmt = $this->mysqli->prepare("INSERT INTO turnos VALUES (NULL, ?, ?)");
            $stmt->bind_param('ss',$date, $owner);
            $stmt->execute();
            if($stmt)   return true;
            return false;
        }

        public function updateAppointment($date, $owner){
            $stmt = $this->mysqli->prepare("UPDATE turnos SET date= ? WHERE owner= ? ;");
            $stmt->bind_param('ss',$date, $owner);
            $stmt->execute();
            if($stmt)   return true;
            return false;
        }

        public function deleteAppointment($owner){
            $stmt = $this->mysqli->prepare("DELETE FROM turnos WHERE owner = ?");
            $stmt->bind_param('s',$owner);
            $stmt->execute();
            if($stmt)   return true;
            return false;
        }

        public function insertVehicle($plate, $model, $mileage, $owner){
            $stmt = $this->mysqli->prepare("INSERT INTO vehiculos VALUES(?, ?, ?, ?);");
            $stmt->bind_param('ssss',$plate, $model, $mileage, $owner);
            $stmt->execute();
            if($stmt)   return true;
            return false;
        }

        public function updateVehicle($plate, $model, $mileage, $owner){
            $str = "UPDATE vehiculos SET model=?, mileage= ? WHERE owner= ? AND plate= ? ;";
            $stmt = $this->mysqli->prepare($str);
            $stmt->bind_param('ssss', $model, $mileage, $owner,$plate);
            $stmt->execute();
            if($stmt)   return true;
            return false;
        }

        public function deleteVehicle($plate, $owner){
            $str = "DELETE FROM vehiculos WHERE plate= ? AND owner= ? ;";
            $stmt = $this->mysqli->prepare($str);
            $stmt->bind_param('ss',$plate, $owner);
            $stmt->execute();
            if($stmt)   return true;
            return false;
        }

        private function endQuery($sql){
            $resultado = $this->mysqli->query($sql);
            if (!$resultado) return null;
            $i = 0;
            $elements = [];
            while ($element = $resultado->fetch_assoc()) {
                $elements[$i] = $element;
                $i +=1;
            }
            return $elements;
        }

        public function queryDivisionSelected($smod){
        
            if($stmt = $this->mysqli->prepare("SELECT coddivision FROM modelos JOIN divisiones ON division=coddivision WHERE nommodelo = ? ;"))
            {
                if($stmt->bind_param('s',$smod)){
                    $sdiv = NULL;
                    if($stmt->execute()){
                        $coddivision = NULL;
                        if ($stmt->bind_result($coddivision)){
                            while($stmt->fetch()){
                                $sdiv = $coddivision;
                            }
                            return $sdiv;
                        }
                    }else echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                }else echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            } else echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            return NULL;
        }

        public function queryServices(){
            if($stmt = $this->mysqli->prepare("SELECT codservicio, nomservicio, desservicio FROM servicios;"))
            {
                $services = [];
                if($stmt->execute()){
                    $codservicio = NULL;
                    $nomservicio = NULL;
                    $desservicio = NULL;
                    $i = 0;
                    if ($stmt->bind_result($codservicio, $nomservicio, $desservicio)){
                        while($stmt->fetch()){
                            $services[$i] = ['codservicio'=> $codservicio , 'nomservicio'=> $nomservicio, 'desservicio'=> $desservicio];
                            $i +=1; 
                        }
                        return $services;
                    }
                }else echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;   
            } else echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            return NULL;
        }


        public function closeConection(){
            $this->mysqli->close();
        }

        public function getAsignedappointments($owner){
            $dates = new DateTime();
            $curdate = $dates->format('Y-m-j h:i:s'); 
            $dates->modify('+ 30 days');
            $limitdate = $dates->format('Y-m-j h:i:s'); 
            $str = "SELECT date FROM turnos WHERE date> ? AND date<= ? AND owner != ? ;";
            if($stmt = $this->mysqli->prepare($str))
            {
                if($stmt->bind_param('sss',$curdate, $limitdate, $owner)){
                    $i=0;
                    $asignedappos = [];
                    if($stmt->execute()){
                        $dat = NULL;
                        if ($stmt->bind_result($dat)){
                            while($stmt->fetch()){
                                $asignedappos[$i] = $dat;
                                $i +=1;
                            }
                            return $asignedappos;
                        }
                    }else echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                }else echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            } else echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            return NULL;
        }

        public function getActiveWorks($owner){
            $str = "SELECT vehiculo, nomestatus, fechainicion, fechaentrega FROM trabajos JOIN estatus ON estatus=codestatus WHERE cliente= ? ;";
            if($stmt = $this->mysqli->prepare($str))
            {
                if($stmt->bind_param('s', $owner)){
                    $i=0;
                    $works = [];
                    if($stmt->execute()){
                        $vehiculo = NULL;
                        $estatus = NULL;
                        $fechai = NULL;
                        $fechae = NULL;
                        if ($stmt->bind_result($vehiculo, $estatus, $fechai, $fechae)){
                            while($stmt->fetch()){
                                $works[$i] = ['vehiculo' => $vehiculo, 'estatus' => $estatus, 'fechai' => $fechai, 'fechae' => $fechae];
                                $i +=1;
                            }
                            return $works;
                        }
                    }else echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                }else echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            } else echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            return NULL;
        }
        // SELECT vehiculo, nomestatus, fechainicion, fechaentrega FROM trabajos JOIN estatus ON estatus=codestatus WHERE cliente='1085341015';
        // INSERT INTO trabajos VALUES('0000000001', '0000000001', 'GJW45E', '1085341015', '2020-07-13 08:00:00', '2020-07-13 08:00:00', '302001', NULL);
        /*
            $stmt = $this->mysqli->prepare("INSERT INTO usuarios VALUES (?, ?, ?, ?)");
            $rol = '2002';
            $crip_pass = password_hash($pass, PASSWORD_DEFAULT);
            $stmt->bind_param('ssss',$id, $name, $crip_pass, $rol);
            $stmt->execute();
            $stmt->close();
            if($stmt){
                return true;
            }
            return false;
        */
       
    }
    # taller-user taller123
?>
