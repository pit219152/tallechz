<?php

    class WorkshopModel{
        public function getModule($route)    
        {
            if(WorkShopConst::WHITELIST[$route]){
                $module = "views/$route.php";
            }else{
                $module = "views/error.php";   
            }
            return $module;    
        }
    }

?>